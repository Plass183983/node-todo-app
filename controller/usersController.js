import crypto from 'crypto';
import Task from '../models/task.js';
import User from '../models/user.js';
import httpError from '../services/httpErrorService.js'
import validate from '../services/validate.js';
import flash_message from '../services/flashMessageService.js';
import auth from '../middleware/auth.js';
import UserInit from '../models/userInit.js';

class UsersController {
  // ADMIN METHOD
  async getAllUsers(req, res, next) {
    /* TODO: Add user role check for admin only */
    let current_user = res.locals.loggedInUser;
    try {
      let users = await User.find({});
      return res.render('admin/users', {users: users});
    } catch(err) {
      next(err);
    }
  }

  // ADMIN METHOD
  async editUser(req, res, next) {
    let userId = req.params.userId;

    try {
      let user = await User.findOne({_id: userId}).populate('lists');
      let init = await UserInit.findOne({user: user._id}).populate('user');

      return res.render('admin/editCreateUser', {action: '/admin/user/' + user._id + '/edit', user: user, title: 'Nutzer bearbeiten', activationCode: init ? init.code : null});
    } catch(err) {
      next(err);
    }
  }

  // ADMIN METHOD
  async createUser(req, res, next) {
    try {
      return res.render('admin/editCreateUser', {action: '/admin/user/create', title: 'Nutzer anlegen'});
    } catch(err) {
      next(err);
    }
  }

  // ADMIN METHOD
  async saveCreateUser(req, res, next) {
    let {name, username, role} = req.body;

    try {
      let validation = validate(User, req.body);
      // Check if username already used
      let exists = await User.findOne({username: username});
      if(exists) {
        flash_message(res, "error", "Nutzer existiert bereits");
        return res.redirect('/admin/user/create');
      }
      
      //let password = crypto.randomBytes(10).toString('hex');
      let user = await User.create({name: name, username: username, role: role})
      let userInitCode = await crypto.randomBytes(10).toString('hex');
      let userInit = await UserInit.create({code: userInitCode, user: user._id});

      flash_message(res, 'success', 'User erfolgreich angelegt');
      return res.redirect('/admin/user/' + user._id + "/edit");
    } catch(err) {
      next(err);
    }
  }

  // ADMIN METHOD
  async saveEditUser(req, res,next) {
    let current_user = res.locals.loggedInUser;
    let userId = req.params.userId;
    let {name, username, role} = req.body;

    try {
      // Check if user already exists
      let exists = await User.findOne({username: username, _id: {$ne: userId}});
      console.log(exists, userId);
      if(exists) {
        flash_message(res, "error", "Nutzer existiert bereits");
        return res.redirect('/admin/user/' + userId + '/edit');
      }


      let user = await User.findByIdAndUpdate(userId, {name: name, username: username, role: role});

      flash_message(res, 'success', 'Erfolgreich geändert!');
      return res.redirect('/admin/users');
    } catch(err) {
      next(err);
    }
  }

  // ADMIN METHOD
  async deleteUser(req, res, next) {
    let userId = req.params.userId;

    try {
      await User.deleteOne({_id: userId});
      
      flash_message(res, 'success', 'Erfolgreich gelöscht');
      return res.status(200).json({success: true});
    } catch(err) {
      next(err);
    }
  }
}

export default new UsersController();