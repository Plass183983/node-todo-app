import Task from '../models/task.js';
import List from '../models/list.js';
import User from '../models/user.js';
import httpError from '../services/httpErrorService.js';
import validator from '../services/validate.js';
import flash_message from '../services/flashMessageService.js';

class TasksController {
  async getTasks(req, res, next) {
    let listId = req.params.listId;
    let user = res.locals.loggedInUser;
  
    try {
      let list = await List.findOne({_id: listId, members: res.locals.loggedInUser._id}).populate('members');
      if(!list) return httpError('404');

      let tasks = await Task.getSortedAndGrouped(list);

      // If json request only send tasks
      if(req.get('Accept') === 'application/json') {
        return res.status(200).send({success: true, tasks: tasks})
      } else {
        // else render the page
        return res.render('tasks', {tasks: tasks, users: list.members, list: list, currentUser: user});
      }

      return res.render('tasks', {tasks: tasks, users: list.members, list: list, currentUser: user});
    } catch(err) {
      next(err);
    }
  } 

  async createTask(req, res, next) {
    let user = res.locals.loggedInUser; 
    let listId = req.params.listId;
    let title = req.body.title;
  
    try {
      let list = await List.findOne({_id: listId, members: user._id})
      if(!list) httpError('404');

      let validation= validator(Task, req.body);
      if(validation.errors) {
        return res.status(400).send({error: validation.errors});
      }

      let task = await Task.create({title: title, created_by: user._id, list: list._id})
      await List.findByIdAndUpdate( list._id, {$push: { tasks: task._id}}, {new: true, useFindAndModify: false})

      let tasks = await Task.getSortedAndGrouped(list);
      return res.status(200).json({success: true, tasks: tasks});
    } catch(err) {
      next(err);
    }
  }

  async updateTask(req, res, next) {
    let listId = req.params.listId;
    let taskId = req.params.taskId;
    let user = res.locals.loggedInUser;
    let {status, assigned_to, due_date} = req.body;
  
    try {
      let list = await List.findOne({_id: listId, members: user._id, tasks: taskId});
      if(!list) return httpError('404');

      let task = await Task.findOne({_id: taskId}).populate('assigned_to');
      if(!task) return httpError('404');

      // Update following fields
      if(assigned_to) {
        task.assigned_to = assigned_to === 'null' ? [] : await User.findOne({_id: assigned_to, lists: list._id});
      }
      if(status) task.status = status;
      if(due_date) task.due_date = due_date === 'null' ? null : due_date;
      
      let validation = validator(Task, task);
      if(validation.errors) {return res.status(403).send({error: validation.errors})}
  
      task = await task.save();
      await task.populate('assigned_to');
      return res.status(200).json({success: true, task: task});
    } catch(err) {
      next(err);
    }
  }

  async deleteTask(req, res, next) {
    let listId = req.params.listId;
    let taskId = req.params.taskId;
    let user = res.locals.loggedInUser;
  
    try {
      let list = await List.findOne({_id: listId, members: user._id, tasks: taskId})
      if(!list) return httpError('403');
      let task = await Task.findOneAndDelete({_id: taskId, list: list._id});
  
      // Remove reference in list
      let taskIndex = list.tasks.indexOf(task._id);
      list.tasks.splice(taskIndex, 1);
      await list.save();
  
      return res.json({success: true, task: task})
    } catch(err) {
      // When the ID isn't valid, it shows up as an error
      next(err);
    }
  }
}

export default new TasksController();