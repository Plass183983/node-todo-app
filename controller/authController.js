import crypto from 'crypto';
import ListInvitation from '../models/listInvitation.js';
import List from '../models/list.js';
import User from '../models/user.js';
import auth from '../middleware/auth.js';
import jwt from 'jsonwebtoken';
import userInit from '../models/userInit.js';

class AuthController {
  async showLoginPage (req, res) {
    return res.render('loginPage');
  }
  
 async login(req, res) {
    let user = await auth.login(req.body.username, req.body.password);
  
    if(!user) {
      return res.render('loginPage', {error: 'Username oder Passwort falsch!'});
    }
  
    let tokenExp = Math.floor(Date.now() / 1000) + (60 * 60); // One hour
    const token = jwt.sign({
      exp: tokenExp, // milliseconds
      data: {user: user}
    }, process.env.JWT_SECRET);
    
    res.cookie("Authorization", "Bearer " + token, {httpOnly: true, secure:true, maxAge: 60 * 60 * 1000}); //seconds
    return res.redirect("/lists");
  }

  async logout(req, res, next) {
    let user = res.locals.loggedInUser;
    res.clearCookie("Authorization");

    return res.redirect('/login');
  }
}

export default new AuthController();