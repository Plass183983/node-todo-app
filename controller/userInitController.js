import auth from "../middleware/auth.js";
import UserInit from "../models/userInit.js";
import User from "../models/user.js";
import flash_message from "../services/flashMessageService.js";
import validate from '../services/validate.js';

class TasksController {

  /* When visited user can set his password */
  async initUserPage(req, res, next) {
    let initCode = req.params.userInitCode;

    try {
      let init = await UserInit.findOne({code: initCode}).populate('user');
      if(!init) httpError('404');

      return res.render('users/initPage', {initCode: init.code});
    } catch (err) {
      next(err);
    }
  }

  async initUser(req, res, next) {
    let initCode = req.params.userInitCode;
    let {username, name, password, confirm_password} = req.body;

    try {
      let init = await UserInit.findOne({code: initCode}).populate('user');
      if(!init || init.active === false) {return httpError('404');}

      if(init.user.username !== username) {
        flash_message(res, 'error', 'Benutzername wurde nicht gefunden. Bitte gib den Benutzernamen an den du vom Admin erhalten hast!');
        return res.redirect('/user/init/' + initCode);
      }

      if(!password || password.length < 8) {
        flash_message(res, 'error', 'Password muss mindestens 8 Zeichen lang sein');
        return res.redirect('/user/init/' + initCode);
      }

      console.log('init', req.body);

      if(password !== confirm_password) {
        flash_message(res, 'error', 'Passwörter müssen übereinstimmen');
        return res.redirect('/user/init/' + initCode);
      }

      let cryptedPassword = await auth.createPassword(password);
      let validated = validate(User, req.body);
      await User.findOneAndUpdate({_id: init.user._id}, {name: validated.name, password: cryptedPassword});
      await UserInit.deleteOne({_id: init._id});

      flash_message(res, 'success', 'Erfolgreich. Logge dich jetzt an!');
      return res.redirect('/login');
    } catch(err) {
      next(err);
    }
    return null;
  }
}

export default new TasksController();