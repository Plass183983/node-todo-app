import crypto from 'crypto';
import ListInvitation from '../models/listInvitation.js';
import List from '../models/list.js';
import User from '../models/user.js';
import Task from '../models/task.js';
import flash_message from '../services/flashMessageService.js';
import httpError from '../services/httpErrorService.js';
import validate from '../services/validate.js';

class ListsController {
  async getLists(req, res, next) {
    try {
      //Gets the current user and its fields
      let user = await User.findById({_id: res.locals.loggedInUser._id}).populate({path: 'lists', populate: {path: 'tasks'}});
      if(!user) httpError("403");

      // count the open tasks and attach them to each list and calc the done/open ratio
      user.lists.forEach((list) => {
        list.closedTasks = 0;
        list.tasks.forEach((task) => {
          list.closedTasks += task.status === 'closed' ? 1 : 0;
        })

        list.closedPercentage = (list.closedTasks > 0 && list.tasks.length > 0) ? Math.floor((list.closedTasks / list.tasks.length) * 100) : 0;
      })

      return res.render('lists', { lists: user.lists, currentUser: user});
    } catch(err) {
      next(err);
    }
  }

  async deleteList(req, res, next) {
    let listId = req.params.listId;
    let user = res.locals.loggedInUser;
  
    try {
      let list = await List.findOne({_id: listId, owner: res.locals.loggedInUser._id}).populate('members');
      if(!list) return httpError("403");
  
      // Delete every task in that list
      await Task.deleteMany({list: list._id});
  
      // Delete list reference from members
      for(let i = 0; i < list.members.length; i++) {
        let listIndex = list.members[i].lists.indexOf(list._id);
        list.members[i].lists.splice(listIndex, 1);
        await list.members[i].save();
      }
  
      await List.deleteOne({_id: list._id});
    } catch(err) {
      next(err);
    }
  
    flash_message(res, 'success', 'Liste erfolgreich gelöscht');
    return res.status(200).send({success: true});
  }

  async createList(req, res, next) {
    let user = res.locals.loggedInUser;

    try {
      let validation = validate(List, {name: req.body.list_name});
      if(validation.errors) { 
        flash_message(res, 'error', validation.errors.name.message);
        return res.redirect('/lists'); 
      }

      let list = await List.create({name: req.body.list_name, members: user._id, owner: user._id});
      let userList = await User.findByIdAndUpdate(user._id, {$push: {lists: list._id}});
    } catch(err) {
      next(err)
    }
    
    flash_message(res, 'success', 'Liste erstellt');
    return res.redirect('/lists');
  }

  async leaveList(req, res) {
    let currentUser = res.locals.loggedInUser;
    let listId = req.params.listId;
    let corrId = res.locals.loggedInUser._id.toString().replace(/.$/,"a");


    try {
      let user = await User.findById(currentUser._id);
      if(!user) return httpError("403");

      let list = await List.findOne({_id: listId, member: user._id, owner: {$ne: user._id}});
      if(!list) return httpError("404");

      let userIndex = list.members.indexOf(user._id);
      if(userIndex >= 0) await list.members.splice(userIndex, 1);

      let listIndex = user.lists.indexOf(list._id);
      if(listIndex >= 0) await user.lists.splice(listIndex, 1);

      await list.save();
      await user.save();

      flash_message(res, 'success', 'Liste verlassen');
      return res.status(200).send({success: true});
    } catch (err) {
      next(err);
    }
  }

  async removeFromList(req, res) {
    let user = res.locals.loggedInUser;
    let listId = req.params.listId;
    let memberId = req.body.member;

    try {
      let list = await List.findOne({_id: listId, owner: user._id, members: memberId});
      if(!list) return httpError("403");

      let member = await User.findOne({_id: memberId, lists: list._id});
      if(!member) return httpError("403");

      let memberIndex = list.members.indexOf(member._id);
      if(memberIndex >= 0) {
        list.members.splice(memberIndex, 1);
        list.save();
      }

      let listIndex = member.lists.indexOf(list._id);
      if(listIndex >= 0) {
        member.lists.splice(listIndex, 1);
        member.save();
      }

      flash_message(res, 'success', 'Mitglied aus Liste entfernt');
      return res.send({success: true});
    } catch(err) {
      next(err);
    }
  }
}

export default new ListsController();