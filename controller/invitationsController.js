import crypto from 'crypto';
import ListInvitation from '../models/listInvitation.js';
import List from '../models/list.js';
import User from '../models/user.js';
import MongooseError from '../errors/MongooseError.js';
import flash_message from '../services/flashMessageService.js';
import httpError from '../services/httpErrorService.js';

class InvitationsController {
  async createInvitation(req, res) {
    let listId = req.params.listId;
    let currentUser = res.locals.loggedInUser;

    try {
      let code = crypto.randomBytes(20).toString('hex');
      let list = await List.findOne({_id: listId, members: currentUser._id});
      let invitation = await ListInvitation.create({code: code, list: list._id});

      return res.status(200).send({success: true, code: invitation.code})
    } catch(err) {
      console.log(err);
      return res.status(500).send({success: false, error: "Es ist ein Fehler aufgetreten"})
    }
  }

  async accessInvitation(req, res) {
    let listInvitationCode = req.params.listInvitationCode;
    
    try {
      let currentUser = await User.findOne({_id: (res.locals.loggedInUser._id)})
      let listInvitation = await ListInvitation.findOne({code: listInvitationCode});
      let list = await List.findOne({_id: listInvitation.list});

      // Check time differenece between now and created_at date in minutes
      let timeDiffNowAndCreatedAt = new Date(Date.now() - listInvitation.created_at.getTime()).getMinutes();

      // Invitaion is valid when: it's not older than 60 minutes or the active flag has been set to false;
      let isValid = timeDiffNowAndCreatedAt <= 60 && listInvitation.active;  
      if(!isValid) httpError("404");

      // Check if user accessing invitation is already member
      if(list.members.includes(currentUser._id)) {
        return res.send("Du bist bereits Mitglied dieser Liste");
      }

      list.members.push(currentUser._id);
      currentUser.lists.push(list._id);
      listInvitation.active = false;
      await listInvitation.remove();

      await list.save();
      await currentUser.save();
      //await listInvitation.save();

      flash_message(res, 'success', 'In Liste aufgenommen!');
      return res.redirect(`/list/${list._id}/tasks`);
    } catch(err) {
      next(err);
      //return res.status('404').send("Einladung wurde nicht gefunden oder ist nicht mehr gültig");
    }
  } 
}

export default new InvitationsController();