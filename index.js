import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import path, {dirname} from 'path';
import expressLayouts from 'express-ejs-layouts';
import { fileURLToPath } from 'url';
import router from './router/router.js';
import db from './db/db.js';
import flashMessagesMiddleware from './middleware/flashMessages.js';
import exceptionHandler from './middleware/exceptionHandler.js';
import validationErrorHandler from './middleware/validationErrorHandler.js';
import httpErrors from './errors/httpErrors.js';

const app = express();
const port = process.env.PORT;
// get paths relative to the file currently executing
const __filename = fileURLToPath(import.meta.url); //gibt den pfad des aktuell aufgerufenen Moduls zurück, um den Dateipfad /path/to/your/script.js aus der URL file:///path/to/your/script.js zu extrahieren.
const __dirname = dirname(__filename); // extrahiert den Pfad aus dem Dateinamen

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/public', express.static(__dirname + '/public'));
app.use(flashMessagesMiddleware);

// Set view engine
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use("", router);
app.use('*', (req, res) => {res.render('errors', {error: {code: 404, message: httpErrors[404]}})})
app.use(validationErrorHandler);
app.use(exceptionHandler);

// connect to mongodb
const connection = db.connect(process.env.DB_USER, process.env.DB_PW);

app.listen(port, () => {
  console.log(`Now listening on port: ${port}`)
} )

