async function sendFetch(url, body, method ) {
  let resp = await fetch(url, {
    headers:  {'Content-Type': 'application/json', 'Accept': 'application/json'},
    method: method,
    body: JSON.stringify(body),
  });
  console.log(resp);
  let val = await resp.json();
  return val;
}