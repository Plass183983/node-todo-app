/***********************
 * 
 *  Deprecated: DO NOT USE ANYMORE
 *  Write own implementation for each modal instead
 * 
 * ********************/

var modalHandler = (function() {
  let modal = document.querySelector('.modal');
  
  /* Sets the event listener on the provided element */
  var onModalOpen = function(modalOpener, callback) {
    document.querySelector(modalOpener).addEventListener('click', () => {
      openModal();
      callback();
    });
  }

  var openModal = function() {
    clearModal();
    modal.showModal();

    let modalCloser = modal.querySelector('.close-modal');
    if(modalCloser) {
      modal.querySelector('.close-modal').addEventListener('click', () => {
        modal.close();
      })
    }
  }

  // Set the content body of modal
  var setBody = function(content) {
    let modalBody = modal.querySelector('.modal-body');
    setContent(modalBody, content);
  }

  // Set the header of the modal
  var setHeader = function(content) {
    let modalHeader = modal.querySelector('.modal-header');
    let modalTitle = document.createElement('h2');
    modalTitle.innerHTML = '';
    modalTitle.innerHTML = content;

    modalHeader.appendChild(modalTitle);
  }

  // Set content of modal footer
  var setFooter = function(content) {
    let modalFooter = modal.querySelector('.modal-footer');
    setContent(modalFooter, content);
  }

  var setContent = function(element, content) {
    //console.log(content)
    if(content instanceof Element || content instanceof HTMLElement || content instanceof DocumentFragment){
      element.appendChild(content); 
    } else {
      element.innerHTML = content;
    }
  }

  var clearModal = function() {
    modal.querySelector('.modal-header').innerHTML = "";
    modal.querySelector('.modal-body').innerHTML = "";
    modal.querySelector('.modal-footer').innerHTML = "";
  }

  return {
    onModalOpen: onModalOpen,
    openModal: openModal,
    setBody: setBody,
    setHeader: setHeader,
    setFooter: setFooter,
    modal: modal,
  }
})();

export default modalHandler;