import modalHandler from "./modalHandler.js";

var listMembersModalHandler = (function () {
  let template = document.getElementById('list-members-modal-content');

  modalHandler.onModalOpen('#show-list-members-list', () => {
    let content = setContent();

    modalHandler.setBody(content);
    modalHandler.setHeader("Alle Mitglieder der Liste");
  })

  var setContent = function() {
    let contentClone = template.content.cloneNode(true);
    let removeBtns = contentClone.querySelectorAll('.remove-member');
    let listId = contentClone.querySelector('#listId').value;

    contentClone.querySelector('.close-modal').addEventListener('click', () => {
      modalHandler.modal.close();
    })

    for(let i = 0; i < removeBtns.length; i++) {
      removeBtns[i].addEventListener('click', (el) => {
        if(confirm('Nutzer wirklich entfernen?')) {
          let url = window.location.origin + "/list/" + listId + "/removeMember";
          let body = {member: el.target.dataset.memberid}

          try {
            sendFetch(url, body, 'PUT').then( (res) => {
              console.log(res);
              if(res.success) {
                let removable = el.target.closest('li');
                removable.parentElement.removeChild(removable);
              }
              if(res.error) {ErrorHandler.toggleErrorBox(res.error); modal.close();}
            });
          } catch(error) {
            ErrorHandler.toggleErrorBox("Es ist ein unerwarteter Fehler aufgetreten, " + error.statusCode);
            modal.close();
            console.log(error)
          }
        }
      });
    }

    return contentClone;
  }
})();