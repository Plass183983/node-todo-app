// Cannot use global modal handler because of the module scope.
// Task-cards using the TaskModalHandler are using the "onclick"-function which cannot
// access the module scope
var TaskModalHandler = (function() {
  let modal = document.querySelector('.modal');
  let template = document.getElementById('task-options-modal-content');
  let contentClone;
  let header;
  let form;
  let taskAssigner;
  let dueDate;
  let taskIdField; 
  let submitForm;
  let deleteBtn;
  let closeBtn;
  let modalFooter;
  let modalBody;

  var openModal = function(el) {
    intialize();

    modal.showModal();
    displayModalInformation(el);
  }

  var intialize = function() {
    // Modal
    header = modal.querySelector('.modal-header');  
    modalBody = modal.querySelector('.modal-body');
    modalFooter = modal.querySelector('.modal-footer');

    // delete all content that was in the modal before
    clearModal();

    // Task form
    contentClone = template.content.cloneNode(true);
    form = contentClone.querySelector('#task-options-form');
    taskAssigner = contentClone.querySelector('#task-assigner-modal');
    dueDate = contentClone.querySelector('#task-due-date-modal');
    taskIdField = contentClone.querySelector('#task-id-modal');
    submitForm = contentClone.querySelector('#submit-form');
    deleteBtn = contentClone.querySelector('#delete-task-modal');
    closeBtn = contentClone.querySelector('#close-modal');

    // Prevent form from submitting
    form.onsubmit = (ev) => ev.preventDefault();

    // Close modal
    closeBtn.onclick = (ev) => modal.close();

    // Delete Task Event
    deleteBtn.onclick = async (ev) => {
      let res = await TaskMaker.deleteTask(taskIdField.value);
      modal.close();
      if(res) return;
    }

    // Trigger fetch-request instead of submitting form and save changes
    submitForm.onclick = async function(ev) {
      ev.preventDefault();
      let res = await TaskMaker.submitTaskUpdate(taskIdField.value, getCurrentModalFormData());
      if(res.task) modal.close();
    }
  }

  var displayModalInformation = function(el) {
    let title = document.createElement("h2");
    let taskCard = el.closest('.task-card');
    let taskId = taskCard.querySelector('#task-id').value;
    let task = TaskMaker.findTask(taskId);
    title.innerHTML = "Task bearbeiten";
    header.appendChild(title);   

    if(task) {
      taskIdField.value = taskId;
      taskAssigner.value = task.assigned_to.length > 0 ? task.assigned_to[0]._id : null;
      dueDate.valueAsDate = task.due_date ? new Date(task.due_date) : null;
    }

    modalBody.appendChild(contentClone);
  }

  var getCurrentModalFormData = function() {
    return {
      assigned_to: taskAssigner.value,
      due_date: dueDate.value,        
    }
  }

  var clearModal = function() {
    if(header) header.innerHTML = "";
    if(modalBody) modalBody.innerHTML = "";
    if(modalFooter) modalFooter.innerHTML = "";
  }

  return {openModal: openModal}
})();