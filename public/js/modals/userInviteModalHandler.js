var userInviteModalHandler = (function() {
  let modal = document.querySelector('.modal');
  let header = modal.querySelector('.modal-header');
  let body = modal.querySelector('.modal-body');
  let footer = modal.querySelector('.modal-footer');
  let content;
  let input;

  var openModal = function() {
    clearModal();
    modal.showModal();

    let title = document.createElement("h2");
    title.innerHTML = "Nutzer einladen";
    header.appendChild(title);
  
    generateBodyInformation();
  }

  var clearModal = function() {
    header.innerHTML = "";
    footer.innerHTML = "";
    body.innerHTML = "";
  }

  var generateBodyInformation = function() {
    let template = document.getElementById('invite-user-template');
    content = template.content.cloneNode(true);
    input = content.querySelector('input');
    
    content.querySelector('.close-modal').onclick = () => {
      modal.close();
    };

    content.querySelector('#request-invite-code').addEventListener('click', (ev) => {
      let listId = ev.target.dataset.list;
      requestInviteCode(listId);
    });

    body.appendChild(content);
  }

  var requestInviteCode = function(listId) {
    let url =  window.location.origin + '/list/' + listId + '/invitation/create';
    try {
      sendFetch(url, {}, 'POST').then( (res) => {
        if(res.success) {
          input.value = window.location.origin + "/invitation/" + res.code;
        }
        if(res.error) {ErrorHandler.toggleErrorBox(res.error); modal.close();}
      });
    } catch(error) {
      ErrorHandler.toggleErrorBox("Es ist ein unerwarteter Fehler aufgetreten, " + error.statusCode);
      modal.close();
      console.log(error)
    }
  }

  return {openModal: openModal}
})();
