var MessageHandler = (function () {
  var toggleErrorBox = function (error) {
    let messageBox = document.querySelector('.messages');
    messageBox.innerHTML = "";

    if(error) {
      if(typeof error === 'object') {
        let ul = document.createElement('ul');
        for(const [key, val] of Object.entries(error)) {
          let li = document.createElement('li');
          li.innerHTML = `${key}: ${error[key].message}`;
          ul.appendChild(li);
        }
        messageBox.appendChild(ul);
      } else {
        messageBox.innerHTML = error;
      }
      messageBox.classList.add('error-box')
    } else {
      if(messageBox) messageBox.classList.remove('error-box');
    }
  }

  return {toggleErrorBox: toggleErrorBox}
})();