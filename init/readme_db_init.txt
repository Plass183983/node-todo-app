1. Set auth
- Open mongosh
use admin
db.createUser(
  {
    user: "myUserAdmin",
    pwd: "admin",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, 
             { role: "dbAdminAnyDatabase", db: "admin" }, 
             { role: "readWriteAnyDatabase", db: "admin" } ]
  }
)
- create desired db
use *name*

- create user 
db.createUser( {user: "todoListAdmin", pwd: "todolist_2024", roles: [ { role: 'readWrite', db:'todo'} ]

- add auth tag to docker-compose
command: [--auth]

- choose database and login
db.auth(name, pw)


- ########## you can also initally create an admin user using the docker-compose file #########
environment:
      # provide your credentials here
      - MONGO_INITDB_ROOT_USERNAME=root
      - MONGO_INITDB_ROOT_PASSWORD=rootPassXXX