import mongoose from 'mongoose';

const urlParamsHandler = async (req, res, next) => {
  if(Object.keys(req.params).length != 0) {
    for(const [key, value] of Object.entries(req.params)) {

      // set first letter of key to uppercase;
      let keyCapitalized = key.charAt(0).toUpperCase() + key.slice(1);

      // Compare mongoose models with provided model name in url
      try {
        res.locals[key] = await mongoose.model(keyCapitalized).findById(value);
      } catch(err) {
        console.log(err);
        return res.sendStatus(404);
      }
    }
  }
  return next();
}

export default urlParamsHandler;