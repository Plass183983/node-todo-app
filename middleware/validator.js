export function requestSanitizer(req, res, next) {
  // Removes html, javascript and $ from string
  let regex = /^\s+|\s+$|<[^>]+>|<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>|(\$)/gi;
  if(req.body) {
    for (const [key, value] of Object.entries(req.body)) {
      req.body[key] = value.replace(regex, '');
    }
  }

  next();
}