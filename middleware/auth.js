import User from '../models/user.js';
import * as bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import httpError from '../services/httpErrorService.js';

const errorResponse = function(req, res) {
  if(req.get('Accept') === 'application/json') {
    return res.status(403).json({error: 'Aktion nicht erlaubt!'});
  } else {
    return res.redirect('/login');
  }
}

const auth = {
  createPassword: (pw)=> {
    return bcrypt
      .hash(pw, 10)
      .then(hash => {
        return hash;
    })
  },
  login: async (username, password) => {
    if(password === '') return null;

    let user = await User.findOne({username: username}).select('+password');

    if(user) {
      try {
        let res = await bcrypt.compare(password, user.password)
       
        if(res) {
          user = user.toObject();
          delete user.password;
          delete user.lists;
          return user;
        }
      } catch(err) {
        console.log(err);
        throw(err);
      }
    }
    return null;
  },
  loggedIn: async function(req, res, next) {
    let cookie = req.headers.cookie;
 
    if(!cookie) {
      return errorResponse(req, res);
    }

    const cookies = cookie.split('; ');
    const authCookie = cookies.find(cookie => cookie.startsWith('Authorization='));

    if(!authCookie) {
      return errorResponse(req, res);
    }

    try {
      const token = authCookie.split('=')[1].replace('Bearer%20', '');
      const result = await jwt.verify(token, process.env.JWT_SECRET);
      res.locals.loggedInUser = User.hydrate(result.data.user);
      return next();
    } catch(err) {
      console.log(err);
      return errorResponse(req, res);
    }
  },
  isAdmin: async function(req, res, next) {
    if(!res.locals.loggedInUser || res.locals.loggedInUser.role !== 'Admin') {
      httpError(req, res, 403);
    }

    return next();
  }
}

export default auth;