import flash_message from "../services/flashMessageService.js";
import ValidationError from "../errors/ValidationError.js";

// Handles validationErrors
const validationErrorHandler = function(err, req, res, next) {
  if(err instanceof ValidationError) {
    if(req.get('Accept') === 'application/json') {
      return res.status(400).send({error: err.errorMessages()})
    } 

    flash_message(res, 'error', err.errorMessages());
    return res.redirect(req.originalUrl);
  } 

  next(err);
}

export default validationErrorHandler;