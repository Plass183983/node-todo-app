import MongooseError from '../errors/MongooseError.js';
import mongoose from 'mongoose';
import httpErrors from '../errors/httpErrors.js';
import HttpError from '../errors/HttpError.js';

// Handles httpErrors
const exceptionHandler = function(err, req, res, next) {
  // Default value
  let errorCode = 500;
  let message = httpErrors[errorCode];

  if(err instanceof HttpError) {
    errorCode = err.httpCode;
    message = err.message;
  }

  // TODO: Might remove later and just throw 500
  if(err instanceof mongoose.Error) {
    let mongooseError = new MongooseError(err);
    errorCode = mongooseError.statusCode != '404' ? 500 : 404; // Handle invalid mongoose ID as 404 and the rest as 500
    message = httpErrors[errorCode];
  }

  console.log(err);

  if(req.get('Accept') === 'application/json') {
    return res.status(errorCode).send({error: message})
  } else {
    return res.render('errors', {error: {code: errorCode, message: message}});
  }
}

export default exceptionHandler;