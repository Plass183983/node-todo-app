const flashMessagesMiddleware = function(req, res, next) {
  if(!req.headers.cookie) {
    return next();
  }

  const cookies = req.headers.cookie.split('; ');
  const flashCookie = cookies.find(cookie => cookie.startsWith('flashMessage='));

  if(flashCookie) {
    let message = flashCookie.split('=')[1];
    res.locals.message = JSON.parse(decodeURIComponent(message));
    res.clearCookie('flashMessage');
  }

  return next();
}

export default flashMessagesMiddleware;