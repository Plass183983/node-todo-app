import express from 'express';
import User from '../models/user.js';
import List from '../models/list.js';
import Task from '../models/task.js';
import { requestSanitizer } from '../middleware/validator.js';
import auth from '../middleware/auth.js';
import invitationsController from '../controller/invitationsController.js';
import authController from '../controller/authController.js';
import listsController from '../controller/listsController.js';
import tasksController from '../controller/tasksController.js';
import usersController from '../controller/usersController.js';
import userInitController from '../controller/userInitController.js';


const router = express.Router();

router.get('/login', requestSanitizer, authController.showLoginPage);
router.post('/login', requestSanitizer, authController.login);
router.get('/logout', auth.loggedIn, authController.logout);

router.get('/', auth.loggedIn, (req, res) => {
  return res.redirect('/lists');
})

// List routes
router.get('/lists', auth.loggedIn, listsController.getLists);
router.delete('/list/:listId/delete', auth.loggedIn, listsController.deleteList);
router.post('/lists/create', auth.loggedIn, requestSanitizer, listsController.createList)
router.put('/list/:listId/leave', auth.loggedIn, listsController.leaveList)
router.put('/list/:listId/removeMember', auth.loggedIn, listsController.removeFromList);
// Task routes
router.get('/list/:listId/tasks', auth.loggedIn, tasksController.getTasks);
router.post('/list/:listId/tasks/create', auth.loggedIn, requestSanitizer, tasksController.createTask);
router.put('/list/:listId/tasks/:taskId/update', auth.loggedIn, tasksController.updateTask)
router.delete('/list/:listId/tasks/:taskId/delete', auth.loggedIn, tasksController.deleteTask)
router.get('/list/:listId/tasks/update', auth.loggedIn, tasksController.getTasks);
// Invitation routes
router.post('/list/:listId/invitation/create', auth.loggedIn, invitationsController.createInvitation);
router.get('/invitation/:listInvitationCode', auth.loggedIn, invitationsController.accessInvitation);

// User routes
router.get('/user/init/:userInitCode', userInitController.initUserPage);
router.post('/user/init/:userInitCode', userInitController.initUser);

//Admin route
router.get('/admin/users', auth.loggedIn, auth.isAdmin, usersController.getAllUsers);
router.get('/admin/user/:userId/edit', auth.loggedIn, usersController.editUser);
router.get('/admin/user/create', auth.loggedIn, usersController.createUser);
router.post('/admin/user/:userId/edit', auth.loggedIn, usersController.saveEditUser);
router.post('/admin/user/create', auth.loggedIn, auth.isAdmin, usersController.saveCreateUser);
router.delete('/admin/user/:userId/edit', auth.loggedIn, auth.isAdmin, usersController.deleteUser);


router.get('/reset', auth.loggedIn, async (req, res) => {
  let user = await User.findById(res.locals.user);

  await List.deleteMany({})
  await Task.deleteMany({})
  user.lists = [];
  user.save();

  return res.redirect('/lists');
})

export default router;