 
// Initally inserts the test-data
let user = await User.create( {
  name: "TestUser",
  password: "test",
  created_at: Date.now(),
  update_at: Date.now()
})

let list = await List.create({
  name: "TestList",
  created_at: Date.now(),
  updated_at: Date.now(),
})

let task = await Task.create({
  title: "TestTask",
  due_date: null,
  list: list._id
})

let updatedUser = await User.findByIdAndUpdate(
  user._id,
  {$push: { lists: list._id }},
  { new: true, useFindAndModify: false }
)

let updateList = await List.findByIdAndUpdate( 
  list._id,
  {$push: { members: user._id, tasks: task._id}},
  {new: true, useFindAndModify: false}
)
console.log(updatedUser, updatedList)