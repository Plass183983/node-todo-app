import mongoose from 'mongoose';

class DB {
  connect(user, password) {
    mongoose.connect(process.env.DB_URI, {
      //authSource: "admin",
      user: user,
      pass: password,
    });
    const DB = mongoose.connection;

    DB.on('error', (err)=> {
      console.log('connection failed')
      console.log(err);
    })

    DB.once('open', () => {
      console.log("DB connection established")
    })

    return mongoose.connection;
  }
}

export default new DB()
