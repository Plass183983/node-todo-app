import mongoose from 'mongoose';
import ValidationError from '../errors/ValidationError.js';

export default function(Model, requestBody) {
  let instance = new Model(requestBody);
  let validationError = instance.validateSync();

  if(validationError) throw new ValidationError(validationError.errors);

  return instance;
}