const flash_message = function(res, level, text) {
  let message = {"level": level, "text": text};
  console.log("setting cookie", message);
  res.cookie('flashMessage', JSON.stringify(message), { maxAge: 600000, httpOnly: true });
}

export default flash_message;