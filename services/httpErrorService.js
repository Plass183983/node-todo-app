import HttpError from "../errors/HttpError.js";
import httpErrors from "../errors/httpErrors.js";

const httpError = function(errorCode) {
  if(!httpErrors.hasOwnProperty(errorCode)) {
    errorCode = 500;
  }

  throw new HttpError(errorCode, httpErrors[errorCode]);
}

export default httpError;