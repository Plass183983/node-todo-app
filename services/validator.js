import mongoose from 'mongoose';

export default function(Model, requestBody) {
  let instance = new Model(requestBody);

  let validation = instance.validateSync(); 
  if(!validation) {
    return instance;
  }

  return validation;
}