class ValidationError extends Error {
  constructor(obj) {
    super("Validierungsfehler aufgetreten");
    this.errors = obj
  };

  errorMessages() {
    let errorMessages = {};
    for(const[key, val] of Object.entries(this.errors)) {
      errorMessages[key] = val.properties.message;
    }

    return errorMessages;
  }
}

export default ValidationError;