import httpErrors from "./httpErrors.js";

class MongooseError extends Error {
  constructor(error) {
    super(error.message)
    this.error = error;
    
    this.handleError();
  }

  handleError = function() {
    switch(this.error.name) {
      case "CastError":
        if(this.error.message.startsWith('Cast to ObjectId')) {
          this.message = httpErrors[404];
          this.statusCode = 404;
          break;
        }
      case "ValidationError":
        this.statusCode = 400;
        break;
      case "TypeError" || "Not Allowed":
        this.message = httpErrors[403];
        this.statusCode = 403;
        break;
      default:
        this.statusCode = 500;
        this.message = httpErrors[500]
    }
  }
}

export default MongooseError;