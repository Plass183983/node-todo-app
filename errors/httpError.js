class HttpError extends Error{
  constructor(httpCode, message) {
    super(message);
    this.httpCode = httpCode;
  }
}

export default HttpError;