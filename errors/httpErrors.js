export default {
  404: "Ressource nicht gefunden",
  403: "Aktion nicht erlaubt",
  500: "Server-Fehler",
}