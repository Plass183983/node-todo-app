import mongoose from 'mongoose';

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  name: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    select: false,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
  role: {
    type: String,
    required: true,
    default: 'User'
  },
  lists: [
    {
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'List'
    }
  ]
}, {collection: 'users'})

userSchema.pre('deleteOne', function (next) {
  let id = this.getQuery()._id;
  mongoose.model("List").deleteMany({'owner': id}, function (err, result) {
    if (err) {
      return next(err);
    }

    mongoose.model("List").updateMany({ members: id}, {$pull: {members: id}}, function(err, result) {
      if(err) {
        return next(err);
      }

      console.log("successfully deleted referencess")
      next();
    })
  })
});

export default mongoose.model('User', userSchema);
