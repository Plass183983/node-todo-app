import mongoose from 'mongoose';

const userInitSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  code : {
    type: String,
    required: true,
  },
  active: {
    type: Boolean,
    default: true,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
}, {collection: 'userInit'})

export default mongoose.model('UserInit', userInitSchema);
