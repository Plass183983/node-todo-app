import mongoose from 'mongoose';

const taskSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    minlength: [3, 'Bitte mindestens 3 Zeichen angeben.'],
    maxLength: [100, 'Bitte maximal 100 Zeichen angeben.' ]
  },
  status: {
    type: String,
    required: true,
    default: "open",
    validate: {
      validator: function(v) {
        let valid = ['open', 'closed'];
        return valid.includes(v);
      },
      message: 'Ungültige Eingabe'
    },
  },
  due_date: {
    type: Date,
    required: false,
    /*validate: {
      validator: function(v) {
        return false
      },
      message: props => `${props.value} is not a valid phone number!`,
    },*/
    default: null,
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
  updated_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
  list: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "List"
  },
  assigned_to: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
}, {collection: 'tasks'})

taskSchema.statics.test = function() {
  return "wow";
}

taskSchema.statics.getSortedAndGrouped = async function(list) {
  return await mongoose.model('Task').aggregate([
    { $match: { list: list._id } },
    { $addFields: {
      // Füge ein Feld 'sortDate' hinzu, das das Task-Datum enthält
      sortDate: { $ifNull: ["$due_date", new Date("9000-01-01")] } },
    },
    { $lookup: {
      "from": "users",
      "localField": "assigned_to",
      "foreignField": "_id",
      "as": "assigned_to"
    }},
    {$sort: {sortDate: 1}},
    { $group: { 
      _id: "$status", 
      all: { $push: "$$ROOT" }
    }},
    { $sort: {
      _id: -1,
    }}
  ])
}

export default mongoose.model('Task', taskSchema);
