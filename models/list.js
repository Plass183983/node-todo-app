import mongoose from 'mongoose';

const listSchema = new mongoose.Schema({
  name: {
    type: String,
    maxLength: [50, "Bitte maximal 50 Zeichen angeben."],
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
  updated_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
  members: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
  tasks: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Task"
  }],
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }
}, {collection: 'lists'})

export default mongoose.model('List', listSchema);
