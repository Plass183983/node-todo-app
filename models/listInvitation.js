import mongoose from 'mongoose';

const listInvitationSchema = new mongoose.Schema({
  code: {
    type: String,
    required: true,
  },
  list : {
    type: mongoose.Schema.Types.ObjectId,
    ref: "List",
    required: true,
  },
  active: {
    type: Boolean,
    default: true,
    required: true,
  },
  created_at: {
    type: Date,
    required: true,
    default: Date.now()
  },
}, {collection: 'listInvitations'})

export default mongoose.model('ListInvitation', listInvitationSchema);
